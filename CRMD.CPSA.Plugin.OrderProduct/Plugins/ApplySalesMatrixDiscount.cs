﻿using CRMD.CPSA.Plugin.OrderProduct.Common;
using CRMD.CPSA.Plugin.OrderProduct.Models;
using CRMD.CPSA.Plugin.OrderProduct.Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.OrderProduct
{
    //Run on Sales Order Create/Update - Post-op
    public class ApplySalesMatrixDiscount : PluginBase, IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            this.PostEntityImageKey = "PostImage";
            base.Initialize(serviceProvider);

            if (this.PostEntity == null)
                return;

            if (this.Context.Depth > 2)
                return;

            this.Tracing.Trace("*** Begin : " + this.PostEntity.Id + "***");

            SalesOrderDetail salesOrderDetail = null;

            if (this.IsCreate)
            {
                salesOrderDetail = SalesOrderDetailService.GetSalesOrderDetail(this.PostEntity);
            }

            if(this.IsUpdate)
            {
                this.Tracing.Trace("Resetting product unit price ...");
                salesOrderDetail = SalesOrderDetailService.GetSalesOrderDetailWithPriceLevel(this.Service, this.PostEntity.Id);
                if(salesOrderDetail != null)
                {
                    decimal dcmOrigUnitPrice = ProductPriceLevelService.GetPriceLevelUnitPrice(this.Service, salesOrderDetail.ProductId, salesOrderDetail.PriceLevelId);
                    SalesOrderDetailService.UpdateUnitPrice(this.Service, this.PostEntity.Id, dcmOrigUnitPrice);
                    this.Tracing.Trace("Reset Unit Price : " + dcmOrigUnitPrice.ToString());
                }
            }
                       
            if(salesOrderDetail != null)
            {
                this.Tracing.Trace("Retrieving sales order line items ...");

                List<SalesOrderDetail> lineItemsX = SalesOrderDetailService.GetSalesOrderLineItems(this.Service, salesOrderDetail.SalesOrderId);
                if(lineItemsX != null)
                {
                    this.Tracing.Trace("Line items found : " + lineItemsX.Count);

                    List<SalesOrderDetail> lineItemsY = new List<SalesOrderDetail>();
                    lineItemsY.AddRange(lineItemsX);

                    int index = 1;
                    foreach(var lineItemX in lineItemsX)
                    {
                        foreach(var lineItemY in lineItemsY)
                        {
                            if(lineItemX.ProductId != lineItemY.ProductId)
                            {
                                this.Tracing.Trace(String.Format("({0}) Retrieving sales matrix : {1} - {2}", index, lineItemX.ProductName, lineItemY.ProductName));

                                decimal dcmTotalDiscount = SalesMatrixService.GetTotalDiscountAmount(this.Service, lineItemX.ProductId, lineItemY.ProductId);
                                this.Tracing.Trace("Unit Price : " + lineItemX.UnitPrice.ToString());
                                this.Tracing.Trace("Discount : " + dcmTotalDiscount.ToString());

                                if (dcmTotalDiscount > 0m)
                                {
                                    lineItemX.UnitPrice = lineItemX.UnitPrice - dcmTotalDiscount;
                                    SalesOrderDetailService.UpdateUnitPrice(this.Service, lineItemX.Id, lineItemX.UnitPrice);

                                    this.Tracing.Trace("Sales Matrix Discount applied to " + lineItemX.ProductName);
                                    this.Tracing.Trace("New unit price : " + lineItemX.UnitPrice.ToString());
                                }
                            }
                        }
                    }
                }
                else
                {
                    this.Tracing.Trace("No line items found.");
                }
            }
            else
            {
                this.Tracing.Trace("Parent sales order not found.");
            }

            this.Tracing.Trace("*** End of plugin execution ***");
        }
    }
}
