﻿using CRMD.CPSA.Plugin.OrderProduct.Common;
using CRMD.CPSA.Plugin.OrderProduct.Models;
using CRMD.CPSA.Plugin.OrderProduct.Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.OrderProduct
{
    //Run when a SalesOrderDetail record is deleted, Pre-event
    public class RevertSalesMatrixDiscount : PluginBase, IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            base.PreEntityImageKey = "PreImage";
            base.Initialize(serviceProvider);

            this.Tracing.Trace("Initializing ...");

            if (this.PreEntity == null)
                return;

            this.Tracing.Trace("*** Begin : " + this.PreEntity.Id + "***");

            SalesOrderDetail salesOrderDetail = SalesOrderDetailService.GetSalesOrderDetail(this.PreEntity);
            if (salesOrderDetail != null && salesOrderDetail.SalesOrderId != Guid.Empty)
            {
                this.Tracing.Trace("Retrieving sales order line items ...");

                List<SalesOrderDetail> lineItems = SalesOrderDetailService.GetRelatedSalesOrderLineItems(this.Service, salesOrderDetail.SalesOrderId, this.TargetEntity.Id);
                if(lineItems != null)
                {
                    this.Tracing.Trace("Line items found : " + lineItems.Count);

                    int index = 1;
                    foreach(var lineItem in lineItems)
                    {
                        this.Tracing.Trace(String.Format("({0}) Retrieving sales matrix : {1} - {2}", index, lineItem.ProductName, salesOrderDetail.ProductName));

                        decimal dcmTotalDiscount = SalesMatrixService.GetTotalDiscountAmount(this.Service, lineItem.ProductId, salesOrderDetail.ProductId);
                        this.Tracing.Trace("Unit Price : " + lineItem.UnitPrice.ToString());
                        this.Tracing.Trace("Discount : " + dcmTotalDiscount.ToString());

                        if (dcmTotalDiscount > 0m)
                        {
                            decimal dcmNewUnitPrice = lineItem.UnitPrice + dcmTotalDiscount;
                            SalesOrderDetailService.UpdateUnitPrice(this.Service, lineItem.Id, dcmNewUnitPrice);

                            this.Tracing.Trace("Sales Matrix Discount added back into " + lineItem.ProductName);
                            this.Tracing.Trace("New unit price : " + dcmNewUnitPrice.ToString());
                        }

                        index++;
                    }
                }
                else
                {
                    this.Tracing.Trace("No related line items found.");
                }
            }
            else
            {
                this.Tracing.Trace("Parent sales order not found.");
            }

            this.Tracing.Trace("*** End of plugin execution ***");
        }
    }
}
