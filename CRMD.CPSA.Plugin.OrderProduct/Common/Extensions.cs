﻿using Microsoft.Xrm.Sdk;

namespace CRMD.CPSA.Plugin.OrderProduct.Common
{
    public static class Extensions
    {
        public static bool HasAttributeValue(this Entity entity, string attributeName)
        {
            return (entity != null && entity.Attributes.Contains(attributeName) && entity.Attributes[attributeName] != null);
        }
    }
}
