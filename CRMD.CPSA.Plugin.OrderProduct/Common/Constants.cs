﻿namespace CRMD.CPSA.Plugin.OrderProduct.Common
{
    public static class Constants
    {
        // OrderProduct (salesorderdetail) fields
        public const string salesorderdetail = "salesorderdetail";
        public const string salesorderid = "salesorderid";
        public const string priceperunit = "priceperunit";
        public const string productid = "productid";
        public const string ispriceoverridden = "ispriceoverridden";

        //Sales Matrix
        public const string crmd_amount = "crmd_amount";
    }
}
