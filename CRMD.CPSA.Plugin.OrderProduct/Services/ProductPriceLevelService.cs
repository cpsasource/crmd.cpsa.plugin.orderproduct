﻿using CRMD.CPSA.Plugin.OrderProduct.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.OrderProduct.Services
{
    public static class ProductPriceLevelService
    {
        public static decimal GetPriceLevelUnitPrice(IOrganizationService service, Guid productId, Guid priceLevelId)
        {
            string fetchXML = @"<fetch mapping='logical' version='1.0' distinct='true'>
                                    <entity name='productpricelevel'>
                                        <attribute name='amount' />
                                        <filter type='and'>
                                            <condition attribute='productid' operator='eq' value='" + productId.ToString() + @"' />
                                            <condition attribute='pricelevelid' operator='eq' value='" + priceLevelId.ToString() + @"' />
                                        </filter>
                                    </entity>
                                </fetch>";

            var result = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if (result != null)
            {
                var priceListItem = result.Entities.FirstOrDefault();
                if (priceListItem != null)
                {
                    return priceListItem.GetAttributeValue<Money>("amount").Value;
                }
            }

            return 0m;
        }
    }
}
