﻿using CRMD.CPSA.Plugin.OrderProduct.Common;
using CRMD.CPSA.Plugin.OrderProduct.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.OrderProduct.Services
{
    public static class SalesMatrixService
    {
        public static decimal GetTotalDiscountAmount(IOrganizationService service, Guid mainProductId, Guid relatedProductId)
        {
            decimal totalDiscountAmount = 0m;

            string fetchXML = @"<fetch mapping='logical' version='1.0' distinct='true'>
                                    <entity name='crmd_salematrix'>
                                        <attribute name='crmd_amount' />
                                        <filter type='and'>
                                            <condition attribute='statecode' operator='eq' value='0' />
                                            <condition attribute='crmd_mainproductid' operator='eq' value='" + mainProductId.ToString() + @"' />
                                            <condition attribute='crmd_relatedproductid' operator='eq' value='" + relatedProductId.ToString() + @"' />
                                        </filter>
                                    </entity>
                                </fetch>";

            var result = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if (result != null)
            {
                List<SalesMatrix> lstMatrix = new List<SalesMatrix>();
                foreach (var matrix in result.Entities)
                {
                    if (matrix.HasAttributeValue(Constants.crmd_amount))
                    {
                        totalDiscountAmount += matrix.GetAttributeValue<Money>(Constants.crmd_amount).Value;
                    }
                }

                return totalDiscountAmount;
            }

            return 0m;
        }
    }
}
