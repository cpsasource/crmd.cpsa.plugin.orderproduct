﻿using CRMD.CPSA.Plugin.OrderProduct.Common;
using CRMD.CPSA.Plugin.OrderProduct.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.OrderProduct.Services
{
    public static class SalesOrderDetailService
    {
        public static SalesOrderDetail GetSalesOrderDetail(Entity salesOrderDetail)
        {
            return ToSalesOrderDetailModel(salesOrderDetail);
        }

        public static SalesOrderDetail GetSalesOrderDetailWithPriceLevel(IOrganizationService service, Guid salesOrderDetailId)
        {
            string fetchXML = @"<fetch mapping='logical' version='1.0' distinct='true'>
                                    <entity name='salesorderdetail'>
                                        <attribute name='salesorderdetailid' />
                                        <attribute name='salesorderid' />
                                        <attribute name='priceperunit' />
                                        <attribute name='productid' />
                                        <filter type='and'>
                                            <condition attribute='salesorderdetailid' operator='eq' value='" + salesOrderDetailId.ToString() + @"' />
                                        </filter>
                                        <link-entity name='salesorder' from='salesorderid' to='salesorderid' alias='order'>
                                            <attribute name='pricelevelid' />
                                        </link-entity>
                                    </entity>
                                </fetch>";

            var result = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if(result != null && result.Entities != null)
            {
                var salesOrderDetail = result.Entities.FirstOrDefault();
                if(salesOrderDetail != null)
                {
                    var detail = ToSalesOrderDetailModel(salesOrderDetail);
                    if (salesOrderDetail.HasAttributeValue("order.pricelevelid"))
                    {
                        detail.PriceLevelId = (salesOrderDetail.GetAttributeValue<AliasedValue>("order.pricelevelid").Value as EntityReference).Id;
                    }

                    return detail;
                }
            }

            return null;
        }

        public static List<SalesOrderDetail> GetSalesOrderLineItems(IOrganizationService service, Guid salesOrderId)
        {
            string fetchXML = @"<fetch mapping='logical' version='1.0' distinct='true'>
                                    <entity name='salesorderdetail'>
                                        <attribute name='salesorderdetailid' />
                                        <attribute name='priceperunit' />
                                        <attribute name='productid' />
                                        <filter type='and'>
                                            <condition attribute='salesorderid' operator='eq' value='" + salesOrderId.ToString() + @"' />
                                        </filter>
                                    </entity>
                                </fetch>";

            var result = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if(result != null)
            {
                List<SalesOrderDetail> lstLineItems = new List<SalesOrderDetail>();
                foreach(var lineItem in result.Entities)
                {
                    lstLineItems.Add(ToSalesOrderDetailModel(lineItem));
                }

                return lstLineItems;
            }

            return null;
        }

        public static List<SalesOrderDetail> GetRelatedSalesOrderLineItems(IOrganizationService service, Guid salesOrderId, Guid salesOrderDetailId)
        {
            string fetchXML = @"<fetch mapping='logical' version='1.0' distinct='true'>
                                    <entity name='salesorderdetail'>
                                        <attribute name='salesorderdetailid' />
                                        <attribute name='priceperunit' />
                                        <attribute name='productid' />
                                        <filter type='and'>
                                            <condition attribute='salesorderid' operator='eq' value='" + salesOrderId.ToString() + @"' />
                                            <condition attribute='salesorderdetailid' operator='ne' value='" + salesOrderDetailId.ToString() + @"' />
                                        </filter>
                                    </entity>
                                </fetch>";

            var result = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if (result != null)
            {
                List<SalesOrderDetail> lstLineItems = new List<SalesOrderDetail>();
                foreach (var lineItem in result.Entities)
                {
                    lstLineItems.Add(ToSalesOrderDetailModel(lineItem));
                }

                return lstLineItems;
            }

            return null;
        }

        public static void UpdateUnitPrice(IOrganizationService service, Guid salesOrderDetailId, decimal unitPrice)
        {
            Entity lineItem = new Entity(Constants.salesorderdetail, salesOrderDetailId);
            lineItem[Constants.ispriceoverridden] = true;
            lineItem[Constants.priceperunit] = new Money(unitPrice);
            service.Update(lineItem);
        }
        
        #region Helper Methods
        private static SalesOrderDetail ToSalesOrderDetailModel(Entity salesOrderDetail)
        {
            SalesOrderDetail lineItem = new SalesOrderDetail()
            {
                Id = salesOrderDetail.Id,
            };

            if (salesOrderDetail.HasAttributeValue(Constants.salesorderid))
            {
                lineItem.SalesOrderId = salesOrderDetail.GetAttributeValue<EntityReference>(Constants.salesorderid).Id;
            }

            if(salesOrderDetail.HasAttributeValue(Constants.productid))
            {
                EntityReference productLookup = salesOrderDetail.GetAttributeValue<EntityReference>(Constants.productid);
                lineItem.ProductId = productLookup.Id;
                lineItem.ProductName = productLookup.Name;
            }

            if (salesOrderDetail.HasAttributeValue(Constants.priceperunit))
            {
                lineItem.UnitPrice = salesOrderDetail.GetAttributeValue<Money>(Constants.priceperunit).Value;
            }

            return lineItem;
        }
        #endregion
    }
}
