﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.OrderProduct.Models
{
    public class SalesMatrix
    {
        public Guid MainProductId { get; set; }
        public Guid RelatedProductId { get; set; }
        public decimal DiscountAmount { get; set; }
    }
}
