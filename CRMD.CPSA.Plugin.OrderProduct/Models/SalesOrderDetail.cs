﻿using Microsoft.Xrm.Sdk;
using System;

namespace CRMD.CPSA.Plugin.OrderProduct.Models
{
    public class SalesOrderDetail
    {
        public Guid Id { get; set; }
        public Guid SalesOrderId { get; set; }
        public Guid PriceLevelId { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
